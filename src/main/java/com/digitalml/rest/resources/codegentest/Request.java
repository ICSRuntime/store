package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Request:
{
  "required": [
    "store"
  ],
  "type": "object",
  "properties": {
    "store": {
      "$ref": "Store"
    }
  }
}
*/

public class Request {

	@Size(max=1)
	@NotNull
	private com.digitalml.www.information.model.retail.Store store;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    store = new com.digitalml.www.information.model.retail.Store();
	}
	public com.digitalml.www.information.model.retail.Store getStore() {
		return store;
	}
	
	public void setStore(com.digitalml.www.information.model.retail.Store store) {
		this.store = store;
	}
}