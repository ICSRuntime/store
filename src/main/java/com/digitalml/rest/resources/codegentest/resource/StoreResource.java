package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.StoreService;
	
import com.digitalml.rest.resources.codegentest.service.StoreService.GetASpecificStoreReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.GetASpecificStoreReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.GetASpecificStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByLatitudeAndLongtitudeAndRangeReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByLatitudeAndLongtitudeAndRangeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByPostcodeAndRangeReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByPostcodeAndRangeReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByPostcodeAndRangeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.UpdateStoreReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.UpdateStoreReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.UpdateStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.ReplaceStoreReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.ReplaceStoreReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.ReplaceStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.CreateStoreReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.CreateStoreReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.CreateStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.DeleteStoreReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.DeleteStoreReturnDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.DeleteStoreInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Store
	 * Access and update of store information.
	 *
	 * @author admin
	 * @version 1
	 *
	 */
	
	@Path("/ICS/rest")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class StoreResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(StoreResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private StoreService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private StoreService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof StoreService)) {
			LOGGER.error(implementationClass + " is not an instance of " + StoreService.class.getName());
			return null;
		}

		return (StoreService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificstore
		Gets store details
	*/
	
	@GET
	@Path("/store/{id}")
	public javax.ws.rs.core.Response getaspecificstore(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificStoreInputParametersDTO inputs = new StoreService.GetASpecificStoreInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificStoreReturnDTO returnValue = delegateService.getaspecificstore(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findstore
		Gets a collection of store details filtered 
	*/
	
	@GET
	@Path("/store")
	public javax.ws.rs.core.Response findstore(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindStoreInputParametersDTO inputs = new StoreService.FindStoreInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindStoreReturnDTO returnValue = delegateService.findstore(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findstorebyLatitudeandLongtitudeandRange
		Gets a collection of store details filtered by Latitude and Longtitude and Range
	*/
	
	@GET
	@Path("/store/find/geog")
	public javax.ws.rs.core.Response findstorebyLatitudeandLongtitudeandRange(
		@QueryParam("Latitude") String Latitude,
		@QueryParam("Longtitude") String Longtitude,
		@QueryParam("Range") String Range,
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindStoreByLatitudeAndLongtitudeAndRangeInputParametersDTO inputs = new StoreService.FindStoreByLatitudeAndLongtitudeAndRangeInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setLatitude(Latitude);
		inputs.setLongtitude(Longtitude);
		inputs.setRange(Range);
		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindStoreByLatitudeAndLongtitudeAndRangeReturnDTO returnValue = delegateService.findstorebyLatitudeandLongtitudeandRange(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findstorebyPostcodeandRange
		Gets a collection of store details filtered by Postcode and Range
	*/
	
	@GET
	@Path("/store/find/postcode")
	public javax.ws.rs.core.Response findstorebyPostcodeandRange(
		@QueryParam("Postcode") String Postcode,
		@QueryParam("Range") String Range,
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindStoreByPostcodeAndRangeInputParametersDTO inputs = new StoreService.FindStoreByPostcodeAndRangeInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setPostcode(Postcode);
		inputs.setRange(Range);
		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindStoreByPostcodeAndRangeReturnDTO returnValue = delegateService.findstorebyPostcodeandRange(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updatestore
		Updates store
	*/
	
	@PATCH
	@Path("/store/{id}")
	public javax.ws.rs.core.Response updatestore(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdateStoreInputParametersDTO inputs = new StoreService.UpdateStoreInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdateStoreReturnDTO returnValue = delegateService.updatestore(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replacestore
		Replaces store
	*/
	
	@PUT
	@Path("/store/{id}")
	public javax.ws.rs.core.Response replacestore(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplaceStoreInputParametersDTO inputs = new StoreService.ReplaceStoreInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplaceStoreReturnDTO returnValue = delegateService.replacestore(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createstore
		Creates store
	*/
	
	@POST
	@Path("/store")
	public javax.ws.rs.core.Response createstore(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreateStoreInputParametersDTO inputs = new StoreService.CreateStoreInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreateStoreReturnDTO returnValue = delegateService.createstore(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deletestore
		Deletes store
	*/
	
	@DELETE
	@Path("/store/{id}")
	public javax.ws.rs.core.Response deletestore(
		@PathParam("id")@NotEmpty String id) {

		DeleteStoreInputParametersDTO inputs = new StoreService.DeleteStoreInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeleteStoreReturnDTO returnValue = delegateService.deletestore(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {
            e.printStackTrace();
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}