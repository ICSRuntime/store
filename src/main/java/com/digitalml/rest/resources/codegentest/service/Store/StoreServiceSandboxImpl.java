package com.digitalml.rest.resources.codegentest.service.Store;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import com.digitalml.rest.resources.codegentest.service.StoreService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;

/**
 * Sandbox implementation for: Store
 * Access and update of store information.
 *
 * @author admin
 * @version 1
 *
 */

public class StoreServiceSandboxImpl extends StoreService {
	

    public GetASpecificStoreCurrentStateDTO getaspecificstoreUseCaseStep1(GetASpecificStoreCurrentStateDTO currentState) {
    

        GetASpecificStoreReturnStatusDTO returnStatus = new GetASpecificStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificStoreCurrentStateDTO getaspecificstoreUseCaseStep2(GetASpecificStoreCurrentStateDTO currentState) {
    

        GetASpecificStoreReturnStatusDTO returnStatus = new GetASpecificStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificStoreCurrentStateDTO getaspecificstoreUseCaseStep3(GetASpecificStoreCurrentStateDTO currentState) {
    

        GetASpecificStoreReturnStatusDTO returnStatus = new GetASpecificStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificStoreCurrentStateDTO getaspecificstoreUseCaseStep4(GetASpecificStoreCurrentStateDTO currentState) {
    

        GetASpecificStoreReturnStatusDTO returnStatus = new GetASpecificStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificStoreCurrentStateDTO getaspecificstoreUseCaseStep5(GetASpecificStoreCurrentStateDTO currentState) {
    

        GetASpecificStoreReturnStatusDTO returnStatus = new GetASpecificStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificStoreCurrentStateDTO getaspecificstoreUseCaseStep6(GetASpecificStoreCurrentStateDTO currentState) {
    

        GetASpecificStoreReturnStatusDTO returnStatus = new GetASpecificStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificStoreCurrentStateDTO getaspecificstoreUseCaseStep7(GetASpecificStoreCurrentStateDTO currentState) {
    

        GetASpecificStoreReturnStatusDTO returnStatus = new GetASpecificStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public FindStoreCurrentStateDTO findstoreUseCaseStep1(FindStoreCurrentStateDTO currentState) {
    

        FindStoreReturnStatusDTO returnStatus = new FindStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreCurrentStateDTO findstoreUseCaseStep2(FindStoreCurrentStateDTO currentState) {
    

        FindStoreReturnStatusDTO returnStatus = new FindStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreCurrentStateDTO findstoreUseCaseStep3(FindStoreCurrentStateDTO currentState) {
    

        FindStoreReturnStatusDTO returnStatus = new FindStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreCurrentStateDTO findstoreUseCaseStep4(FindStoreCurrentStateDTO currentState) {
    

        FindStoreReturnStatusDTO returnStatus = new FindStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreCurrentStateDTO findstoreUseCaseStep5(FindStoreCurrentStateDTO currentState) {
    

        FindStoreReturnStatusDTO returnStatus = new FindStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreCurrentStateDTO findstoreUseCaseStep6(FindStoreCurrentStateDTO currentState) {
    

        FindStoreReturnStatusDTO returnStatus = new FindStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO findstorebyLatitudeandLongtitudeandRangeUseCaseStep1(FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO returnStatus = new FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO findstorebyLatitudeandLongtitudeandRangeUseCaseStep2(FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO returnStatus = new FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO findstorebyLatitudeandLongtitudeandRangeUseCaseStep3(FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO returnStatus = new FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO findstorebyLatitudeandLongtitudeandRangeUseCaseStep4(FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO returnStatus = new FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO findstorebyLatitudeandLongtitudeandRangeUseCaseStep5(FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO returnStatus = new FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO findstorebyLatitudeandLongtitudeandRangeUseCaseStep6(FindStoreByLatitudeAndLongtitudeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO returnStatus = new FindStoreByLatitudeAndLongtitudeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public FindStoreByPostcodeAndRangeCurrentStateDTO findstorebyPostcodeandRangeUseCaseStep1(FindStoreByPostcodeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByPostcodeAndRangeReturnStatusDTO returnStatus = new FindStoreByPostcodeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByPostcodeAndRangeCurrentStateDTO findstorebyPostcodeandRangeUseCaseStep2(FindStoreByPostcodeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByPostcodeAndRangeReturnStatusDTO returnStatus = new FindStoreByPostcodeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByPostcodeAndRangeCurrentStateDTO findstorebyPostcodeandRangeUseCaseStep3(FindStoreByPostcodeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByPostcodeAndRangeReturnStatusDTO returnStatus = new FindStoreByPostcodeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByPostcodeAndRangeCurrentStateDTO findstorebyPostcodeandRangeUseCaseStep4(FindStoreByPostcodeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByPostcodeAndRangeReturnStatusDTO returnStatus = new FindStoreByPostcodeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByPostcodeAndRangeCurrentStateDTO findstorebyPostcodeandRangeUseCaseStep5(FindStoreByPostcodeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByPostcodeAndRangeReturnStatusDTO returnStatus = new FindStoreByPostcodeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindStoreByPostcodeAndRangeCurrentStateDTO findstorebyPostcodeandRangeUseCaseStep6(FindStoreByPostcodeAndRangeCurrentStateDTO currentState) {
    

        FindStoreByPostcodeAndRangeReturnStatusDTO returnStatus = new FindStoreByPostcodeAndRangeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public UpdateStoreCurrentStateDTO updatestoreUseCaseStep1(UpdateStoreCurrentStateDTO currentState) {
    

        UpdateStoreReturnStatusDTO returnStatus = new UpdateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateStoreCurrentStateDTO updatestoreUseCaseStep2(UpdateStoreCurrentStateDTO currentState) {
    

        UpdateStoreReturnStatusDTO returnStatus = new UpdateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateStoreCurrentStateDTO updatestoreUseCaseStep3(UpdateStoreCurrentStateDTO currentState) {
    

        UpdateStoreReturnStatusDTO returnStatus = new UpdateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateStoreCurrentStateDTO updatestoreUseCaseStep4(UpdateStoreCurrentStateDTO currentState) {
    

        UpdateStoreReturnStatusDTO returnStatus = new UpdateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateStoreCurrentStateDTO updatestoreUseCaseStep5(UpdateStoreCurrentStateDTO currentState) {
    

        UpdateStoreReturnStatusDTO returnStatus = new UpdateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateStoreCurrentStateDTO updatestoreUseCaseStep6(UpdateStoreCurrentStateDTO currentState) {
    

        UpdateStoreReturnStatusDTO returnStatus = new UpdateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateStoreCurrentStateDTO updatestoreUseCaseStep7(UpdateStoreCurrentStateDTO currentState) {
    

        UpdateStoreReturnStatusDTO returnStatus = new UpdateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public ReplaceStoreCurrentStateDTO replacestoreUseCaseStep1(ReplaceStoreCurrentStateDTO currentState) {
    

        ReplaceStoreReturnStatusDTO returnStatus = new ReplaceStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceStoreCurrentStateDTO replacestoreUseCaseStep2(ReplaceStoreCurrentStateDTO currentState) {
    

        ReplaceStoreReturnStatusDTO returnStatus = new ReplaceStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceStoreCurrentStateDTO replacestoreUseCaseStep3(ReplaceStoreCurrentStateDTO currentState) {
    

        ReplaceStoreReturnStatusDTO returnStatus = new ReplaceStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceStoreCurrentStateDTO replacestoreUseCaseStep4(ReplaceStoreCurrentStateDTO currentState) {
    

        ReplaceStoreReturnStatusDTO returnStatus = new ReplaceStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceStoreCurrentStateDTO replacestoreUseCaseStep5(ReplaceStoreCurrentStateDTO currentState) {
    

        ReplaceStoreReturnStatusDTO returnStatus = new ReplaceStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceStoreCurrentStateDTO replacestoreUseCaseStep6(ReplaceStoreCurrentStateDTO currentState) {
    

        ReplaceStoreReturnStatusDTO returnStatus = new ReplaceStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceStoreCurrentStateDTO replacestoreUseCaseStep7(ReplaceStoreCurrentStateDTO currentState) {
    

        ReplaceStoreReturnStatusDTO returnStatus = new ReplaceStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public CreateStoreCurrentStateDTO createstoreUseCaseStep1(CreateStoreCurrentStateDTO currentState) {
    

        CreateStoreReturnStatusDTO returnStatus = new CreateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateStoreCurrentStateDTO createstoreUseCaseStep2(CreateStoreCurrentStateDTO currentState) {
    

        CreateStoreReturnStatusDTO returnStatus = new CreateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateStoreCurrentStateDTO createstoreUseCaseStep3(CreateStoreCurrentStateDTO currentState) {
    

        CreateStoreReturnStatusDTO returnStatus = new CreateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateStoreCurrentStateDTO createstoreUseCaseStep4(CreateStoreCurrentStateDTO currentState) {
    

        CreateStoreReturnStatusDTO returnStatus = new CreateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateStoreCurrentStateDTO createstoreUseCaseStep5(CreateStoreCurrentStateDTO currentState) {
    

        CreateStoreReturnStatusDTO returnStatus = new CreateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateStoreCurrentStateDTO createstoreUseCaseStep6(CreateStoreCurrentStateDTO currentState) {
    

        CreateStoreReturnStatusDTO returnStatus = new CreateStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public DeleteStoreCurrentStateDTO deletestoreUseCaseStep1(DeleteStoreCurrentStateDTO currentState) {
    

        DeleteStoreReturnStatusDTO returnStatus = new DeleteStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteStoreCurrentStateDTO deletestoreUseCaseStep2(DeleteStoreCurrentStateDTO currentState) {
    

        DeleteStoreReturnStatusDTO returnStatus = new DeleteStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteStoreCurrentStateDTO deletestoreUseCaseStep3(DeleteStoreCurrentStateDTO currentState) {
    

        DeleteStoreReturnStatusDTO returnStatus = new DeleteStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteStoreCurrentStateDTO deletestoreUseCaseStep4(DeleteStoreCurrentStateDTO currentState) {
    

        DeleteStoreReturnStatusDTO returnStatus = new DeleteStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteStoreCurrentStateDTO deletestoreUseCaseStep5(DeleteStoreCurrentStateDTO currentState) {
    

        DeleteStoreReturnStatusDTO returnStatus = new DeleteStoreReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of store information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = StoreService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}