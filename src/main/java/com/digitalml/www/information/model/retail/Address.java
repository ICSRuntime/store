package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Address:
{
  "type": "object",
  "properties": {
    "line1": {
      "type": "string"
    },
    "line2": {
      "type": "string"
    },
    "line3": {
      "type": "string"
    },
    "line4": {
      "type": "string"
    },
    "city": {
      "type": "string"
    },
    "county": {
      "type": "string"
    },
    "stateProvince": {
      "type": "string"
    },
    "postCode": {
      "type": "string"
    },
    "country": {
      "type": "string"
    }
  }
}
*/

public class Address {

	@Size(max=1)
	private String line1;

	@Size(max=1)
	private String line2;

	@Size(max=1)
	private String line3;

	@Size(max=1)
	private String line4;

	@Size(max=1)
	private String city;

	@Size(max=1)
	private String county;

	@Size(max=1)
	private String stateProvince;

	@Size(max=1)
	private String postCode;

	@Size(max=1)
	private String country;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    line1 = null;
	    line2 = null;
	    line3 = null;
	    line4 = null;
	    city = null;
	    county = null;
	    stateProvince = null;
	    postCode = null;
	    country = null;
	}
	public String getLine1() {
		return line1;
	}
	
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getLine3() {
		return line3;
	}
	
	public void setLine3(String line3) {
		this.line3 = line3;
	}
	public String getLine4() {
		return line4;
	}
	
	public void setLine4(String line4) {
		this.line4 = line4;
	}
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	
	public void setCounty(String county) {
		this.county = county;
	}
	public String getStateProvince() {
		return stateProvince;
	}
	
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	public String getPostCode() {
		return postCode;
	}
	
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
}