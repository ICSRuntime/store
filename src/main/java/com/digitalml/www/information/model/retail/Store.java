package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Store:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "type": {
      "type": "string"
    },
    "status": {
      "type": "string"
    },
    "contact": {
      "type": "string"
    },
    "manager": {
      "type": "string"
    },
    "address": {
      "$ref": "Address"
    },
    "format": {
      "type": "string"
    }
  }
}
*/

public class Store {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String type;

	@Size(max=1)
	private String status;

	@Size(max=1)
	private String contact;

	@Size(max=1)
	private String manager;

	@Size(max=1)
	private com.digitalml.www.information.model.retail.Address address;

	@Size(max=1)
	private String format;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    name = null;
	    type = null;
	    status = null;
	    contact = null;
	    manager = null;
	    address = new com.digitalml.www.information.model.retail.Address();
	    format = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getContact() {
		return contact;
	}
	
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getManager() {
		return manager;
	}
	
	public void setManager(String manager) {
		this.manager = manager;
	}
	public com.digitalml.www.information.model.retail.Address getAddress() {
		return address;
	}
	
	public void setAddress(com.digitalml.www.information.model.retail.Address address) {
		this.address = address;
	}
	public String getFormat() {
		return format;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
}