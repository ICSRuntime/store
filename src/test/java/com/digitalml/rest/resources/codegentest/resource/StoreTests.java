package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class StoreTests {

	@Test
	public void testResourceInitialisation() {
		StoreResource resource = new StoreResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetASpecificStoreNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.getaspecificstore(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetASpecificStoreNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getaspecificstore(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetASpecificStoreAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getaspecificstore(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationFindStoreNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.findstore(0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindStoreNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.findstore(0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindStoreAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.findstore(0, 0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationFindStoreByLatitudeAndLongtitudeAndRangeNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.findstorebyLatitudeandLongtitudeandRange(null, null, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindStoreByLatitudeAndLongtitudeAndRangeNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.findstorebyLatitudeandLongtitudeandRange(null, null, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindStoreByLatitudeAndLongtitudeAndRangeAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.findstorebyLatitudeandLongtitudeandRange(null, null, null, 0, 0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationFindStoreByPostcodeAndRangeNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.findstorebyPostcodeandRange(null, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindStoreByPostcodeAndRangeNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.findstorebyPostcodeandRange(null, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationFindStoreByPostcodeAndRangeAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.findstorebyPostcodeandRange(null, null, 0, 0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateStoreNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.updatestore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateStoreNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updatestore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateStoreAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updatestore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationReplaceStoreNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.replacestore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationReplaceStoreNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.replacestore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationReplaceStoreAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.replacestore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateStoreNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.createstore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateStoreNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createstore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateStoreAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createstore(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteStoreNoSecurity() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(null);

		Response response = resource.deletestore(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteStoreNotAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deletestore(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteStoreAuthorised() {
		StoreResource resource = new StoreResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deletestore(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}