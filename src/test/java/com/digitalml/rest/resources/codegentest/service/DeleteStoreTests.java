package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.StoreService.DeleteStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.DeleteStoreReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteStoreTests {

	@Test
	public void testOperationDeleteStoreBasicMapping()  {
		StoreServiceDefaultImpl serviceDefaultImpl = new StoreServiceDefaultImpl();
		DeleteStoreInputParametersDTO inputs = new DeleteStoreInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		DeleteStoreReturnDTO returnValue = serviceDefaultImpl.deletestore(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}