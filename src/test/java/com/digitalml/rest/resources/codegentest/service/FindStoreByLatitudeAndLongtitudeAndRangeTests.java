package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByLatitudeAndLongtitudeAndRangeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByLatitudeAndLongtitudeAndRangeReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindStoreByLatitudeAndLongtitudeAndRangeTests {

	@Test
	public void testOperationFindStoreByLatitudeAndLongtitudeAndRangeBasicMapping()  {
		StoreServiceDefaultImpl serviceDefaultImpl = new StoreServiceDefaultImpl();
		FindStoreByLatitudeAndLongtitudeAndRangeInputParametersDTO inputs = new FindStoreByLatitudeAndLongtitudeAndRangeInputParametersDTO();
		inputs.setLatitude(null);
		inputs.setLongtitude(null);
		inputs.setRange(null);
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindStoreByLatitudeAndLongtitudeAndRangeReturnDTO returnValue = serviceDefaultImpl.findstorebyLatitudeandLongtitudeandRange(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}