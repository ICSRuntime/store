package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByPostcodeAndRangeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreByPostcodeAndRangeReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindStoreByPostcodeAndRangeTests {

	@Test
	public void testOperationFindStoreByPostcodeAndRangeBasicMapping()  {
		StoreServiceDefaultImpl serviceDefaultImpl = new StoreServiceDefaultImpl();
		FindStoreByPostcodeAndRangeInputParametersDTO inputs = new FindStoreByPostcodeAndRangeInputParametersDTO();
		inputs.setPostcode(null);
		inputs.setRange(null);
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindStoreByPostcodeAndRangeReturnDTO returnValue = serviceDefaultImpl.findstorebyPostcodeandRange(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}