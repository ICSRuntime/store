package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.FindStoreReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindStoreTests {

	@Test
	public void testOperationFindStoreBasicMapping()  {
		StoreServiceDefaultImpl serviceDefaultImpl = new StoreServiceDefaultImpl();
		FindStoreInputParametersDTO inputs = new FindStoreInputParametersDTO();
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindStoreReturnDTO returnValue = serviceDefaultImpl.findstore(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}