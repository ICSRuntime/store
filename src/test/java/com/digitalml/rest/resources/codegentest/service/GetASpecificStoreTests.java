package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.StoreService.GetASpecificStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.GetASpecificStoreReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificStoreTests {

	@Test
	public void testOperationGetASpecificStoreBasicMapping()  {
		StoreServiceDefaultImpl serviceDefaultImpl = new StoreServiceDefaultImpl();
		GetASpecificStoreInputParametersDTO inputs = new GetASpecificStoreInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificStoreReturnDTO returnValue = serviceDefaultImpl.getaspecificstore(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}