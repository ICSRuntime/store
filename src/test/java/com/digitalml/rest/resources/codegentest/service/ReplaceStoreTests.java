package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.StoreService.ReplaceStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.ReplaceStoreReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class ReplaceStoreTests {

	@Test
	public void testOperationReplaceStoreBasicMapping()  {
		StoreServiceDefaultImpl serviceDefaultImpl = new StoreServiceDefaultImpl();
		ReplaceStoreInputParametersDTO inputs = new ReplaceStoreInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		ReplaceStoreReturnDTO returnValue = serviceDefaultImpl.replacestore(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}