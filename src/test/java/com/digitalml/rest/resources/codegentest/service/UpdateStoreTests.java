package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Store.StoreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.StoreService.UpdateStoreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.StoreService.UpdateStoreReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UpdateStoreTests {

	@Test
	public void testOperationUpdateStoreBasicMapping()  {
		StoreServiceDefaultImpl serviceDefaultImpl = new StoreServiceDefaultImpl();
		UpdateStoreInputParametersDTO inputs = new UpdateStoreInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		UpdateStoreReturnDTO returnValue = serviceDefaultImpl.updatestore(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}